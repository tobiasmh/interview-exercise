package com.example.marketmaker;

import com.example.marketmaker.service.QuoteRequestService;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServerTest {

    @Test
    public void testValidMessagePayload() throws IOException {
        var quoteRequestService = mock(QuoteRequestService.class);
        when(quoteRequestService.quote(any())).thenReturn(55.0);

        var response = TestUtils.connectToServerWithPayload("123 BUY 678", quoteRequestService);
        assertThat(response).isEqualTo("55.0");
    }

    @Test
    public void invalidPayloadReturnsAnErrorMessage() throws IOException {
        var response = TestUtils.connectToServerWithPayload("INVALID PAYLOAD", mock(QuoteRequestService.class));
        assertThat(response).isEqualTo("ERROR|Only three message tokens can be provided");
    }

}
