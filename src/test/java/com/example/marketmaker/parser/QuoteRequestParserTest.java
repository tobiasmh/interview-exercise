package com.example.marketmaker.parser;

import com.example.marketmaker.model.QuoteRequestType;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class QuoteRequestParserTest {

    private String assertThrowsAndReturnMessage(String input) {
        ParseException exception = assertThrows(ParseException.class, () -> QuoteRequestParser.parse(input));
        return exception.getMessage();
    }

    @Test
    public void nullPayloadRaisesException() {
        String input = null;
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Payload is null");
    }

    @Test
    public void payloadWithLessThanThreeTokensRaisesException() {
        var input = "123 BUY";
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Only three message tokens can be provided");
    }

    @Test
    public void payloadWithMoreThanThreeTokensRaisesException() {
        var input = "123 BUY 1234 222";
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Only three message tokens can be provided");
    }


    @Test
    public void nonNumericSecurityIdRaisesException() {
        var input = "AGT BUY 123";
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Security Id must be a valid integer");
    }

    @Test
    public void nonNumericQuantityRaisesException() {
        var input = "123 BUY A33";
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Quantity must be a valid integer");
    }

    @Test
    public void quoteRequestTypeNotValidOptionRaisesException() {
        var input = "123 SHORT 456";
        assertThat(assertThrowsAndReturnMessage(input)).isEqualTo("Quote request type must be BUY or SELL");
    }

    @Test
    public void validPayloadIsParsedCorrectly() throws ParseException {
        var input = "123 BUY 456";
        var parsedOutput = QuoteRequestParser.parse(input);
        assertThat(parsedOutput.securityId()).isEqualTo(123);
        assertThat(parsedOutput.quoteRequestType()).isEqualTo(QuoteRequestType.BUY);
        assertThat(parsedOutput.quantity()).isEqualTo(456);
    }

}
