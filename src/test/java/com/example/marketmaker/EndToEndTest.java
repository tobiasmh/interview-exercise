package com.example.marketmaker;

import com.example.marketmaker.service.QuoteCalculationEngine;
import com.example.marketmaker.service.QuoteRequestService;
import com.example.marketmaker.service.ReferencePriceSource;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EndToEndTest {

    @Test
    public void endToEndTest() throws IOException {

        var securityId = 12345;
        var buySell = "BUY";
        var quantity = 55;
        var referencePrice = 25.0;

        var referencePriceSource = mock(ReferencePriceSource.class);
        when(referencePriceSource.get(eq(securityId))).thenReturn(referencePrice);

        var quoteCalculationEngine = mock(QuoteCalculationEngine.class);
        when(quoteCalculationEngine.calculateQuotePrice(eq(securityId), eq(referencePrice), eq("BUY".equals(buySell)), eq(quantity))).thenReturn(50.0);

        var quoteRequestService = new QuoteRequestService(referencePriceSource, quoteCalculationEngine);

        var payload = String.format("%s %s %s", securityId, buySell, quantity);
        var response = TestUtils.connectToServerWithPayload(payload, quoteRequestService);

        assertThat(response).isEqualTo("50.0");
    }

}
