package com.example.marketmaker;

import com.example.marketmaker.service.QuoteRequestService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

public class TestUtils {

    public static String connectToServerWithPayload(String payload, QuoteRequestService quoteRequestService) throws IOException {
        var server = new Server(8888, 10, quoteRequestService);

        var serverThread = new Thread(server::runServer);
        serverThread.start();

        await().atMost(5, TimeUnit.SECONDS).ignoreException(ConnectException.class).until(() -> {
            var socket = new Socket("localhost", 8888);
            socket.close();
            return true;
        });

        var socket = new Socket("localhost", 8888);

        var printWriter = new PrintWriter(socket.getOutputStream(), true);
        var bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        printWriter.println(payload);
        printWriter.flush();

        var response = bufferedReader.readLine();

        printWriter.close();
        bufferedReader.close();
        socket.close();
        server.shutdown();
        serverThread.interrupt();

        return response;
    }
}
