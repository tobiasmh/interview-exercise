package com.example.marketmaker.parser;

import com.example.marketmaker.model.QuoteRequest;
import com.example.marketmaker.model.QuoteRequestType;

public class QuoteRequestParser {

    public static QuoteRequest parse(String input) throws ParseException {

        if (input == null) {
            throw new ParseException("Payload is null");
        }

        String[] messageTokens = input.split(" ");
        if (messageTokens.length != 3) {
            throw new ParseException("Only three message tokens can be provided");
        }

        var securityId = parseInteger(messageTokens[0], "Security Id must be a valid integer");
        var quoteRequestType = parseQuoteRequestType(messageTokens[1]);
        var quantity = parseInteger(messageTokens[2], "Quantity must be a valid integer");

        return new QuoteRequest(securityId, quoteRequestType, quantity);
    }

    private static QuoteRequestType parseQuoteRequestType(String input) throws ParseException {
        try {
            return QuoteRequestType.valueOf(input);
        } catch (IllegalArgumentException e) {
            throw new ParseException("Quote request type must be BUY or SELL");
        }
    }

    private static Integer parseInteger(String input, String parseExceptionMessage) throws ParseException {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            throw new ParseException(parseExceptionMessage);
        }
    }
}
