package com.example.marketmaker.parser;

public class ParseException extends Exception {

    public ParseException(String message) {
        super(message);
    }
}
