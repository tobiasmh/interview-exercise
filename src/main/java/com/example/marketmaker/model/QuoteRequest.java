package com.example.marketmaker.model;

public record QuoteRequest(Integer securityId, QuoteRequestType quoteRequestType, Integer quantity) {
}
