package com.example.marketmaker.model;

public enum QuoteRequestType {
    BUY,
    SELL
}
