package com.example.marketmaker;

import com.example.marketmaker.parser.ParseException;
import com.example.marketmaker.parser.QuoteRequestParser;
import com.example.marketmaker.service.QuoteRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final Integer port;
    private final QuoteRequestService quoteRequestService;
    private final ExecutorService executorService;
    private Boolean shouldShutdown = false;
    private ServerSocket server;

    private static final Logger logger = LoggerFactory.getLogger(Server.class);

    public Server(Integer port, Integer numberOfThreads, QuoteRequestService quoteRequestService) {
        this.port = port;
        this.quoteRequestService = quoteRequestService;
        this.executorService = Executors.newFixedThreadPool(numberOfThreads);
    }

    public void runServer() {

        try {
            server = new ServerSocket(port);
            server.setReuseAddress(true);

            while (!shouldShutdown) {
                var socket = server.accept();
                Handler handler = new Handler(socket);
                this.executorService.submit(handler);
            }
        } catch (IOException e) {
            logger.warn("Server shutdown with exception", e);
        }
    }

    public void shutdown() throws IOException {
        this.server.close();
        this.shouldShutdown = true;
    }

    private class Handler implements Runnable {
        private final Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        public void run() {
            BufferedReader inputReader = null;
            try (PrintWriter outputWriter = new PrintWriter(socket.getOutputStream(), true)) {
                inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line = inputReader.readLine();

                try {
                    var quoteRequest = QuoteRequestParser.parse(line);
                    var quote = quoteRequestService.quote(quoteRequest);
                    outputWriter.println(quote);
                } catch (ParseException e) {
                    logger.warn("Invalid request payload provided {}", e.getMessage(), e);
                    outputWriter.println(String.format("ERROR|%s", e.getMessage()));
                }
                socket.close();

            } catch (IOException e) {
                logger.info("Socket handler failed with exception ", e);
            } finally {
                if (inputReader != null) {
                    try {
                        inputReader.close();
                        socket.close();
                    } catch (IOException e) {
                        logger.info("Socket handler resource cleanup failed with exception ", e);
                    }
                }

            }
        }
    }

}
