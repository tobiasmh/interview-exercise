package com.example.marketmaker.service;

import com.example.marketmaker.model.QuoteRequest;
import com.example.marketmaker.model.QuoteRequestType;

public class QuoteRequestService {

    private final ReferencePriceSource referencePriceSource;
    private final QuoteCalculationEngine quoteCalculationEngine;

    public QuoteRequestService(ReferencePriceSource referencePriceSource, QuoteCalculationEngine quoteCalculationEngine) {
        this.referencePriceSource = referencePriceSource;
        this.quoteCalculationEngine = quoteCalculationEngine;
    }

    public double quote(QuoteRequest quoteRequest) {

        var referencePrice = referencePriceSource.get(quoteRequest.securityId());

        return quoteCalculationEngine.calculateQuotePrice(quoteRequest.securityId(),
                referencePrice,
                quoteRequest.quoteRequestType() == QuoteRequestType.BUY,
                quoteRequest.quantity());
    }

}
