
## Assessment Notes

* I have kept the complexity of the submission roughly in line with the couple of hours of effort required that was indicated during the initial call I had with Simon.
* I identified three separate concerns from the requirements, I have split these into three separate classes.
    * TCP socket server (Server)
    * Parsing of the request payload (QuoteRequestParser)
    * Interfacing with the quoting engine and reference price source (QuoteRequestService)

### Technical Notes
* The requirement of a large number of quote requests does not provide a specific scalability requirement. The implementation provided uses TCP Sockets with a fixed thread pool.
  This prevents the application from running out of resources due to too many requests. Further improvements to the scalability of the application can be achieved with NIO sockets,
  and more low level optimisations can be made like dropping the use of Class models (i.e. the Record class) to save space on the heap.
* Passing the price around as a float is probably not a good idea. However as the provided interfaces use this, I have kept this as the type for the price.
* The low level socket handling code has multiple points where an IOException can be raised. If the handler thread fails the thread will die, and the client can retry.
  Further improvements could be made to the server to handle a TCP failure and automatically restart. There are a variety of other keep alive strategies that can be used here.
* A number of bound cases are not handled in the implementation, for example if the security is not found or a negative quantity is provided.
* QuoteCalculationEngine and ReferencePriceSource are expected to be thread safe.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------